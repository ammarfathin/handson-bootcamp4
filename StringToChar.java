import java.util.Scanner;

public class StringToChar {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan kata: ");
        String kata = scanner.nextLine();

        for (int i = 0; i < kata.length(); i++) {
            System.out.println(kata.charAt(i));
        }

        scanner.close();
    }
}