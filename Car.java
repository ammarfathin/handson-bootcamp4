public class Car {
    private String name;
    private String color;

    public Car(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public void start() {
        System.out.println("Starting " + name);
        System.out.println("Color: " + color);
    }

    public static void main(String[] args) {
        for (int i = 1; i <= 5; i++) {
            Car car = new Car("Toyota", "Red");

            // Simulate some operations on the hamster object to use more memory
            for (int j = 0; j < 10; j++) {
                car.start();
            }

            car = null;
        }

        System.gc();
    }
}
